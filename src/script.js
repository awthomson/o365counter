// When was the date of the last outage UTC (0 = Jan)
// (Use local date)
var last_outage = new Date(2019,4,3);

// How many days have had an outage this year so far
var days_down = 16;

// Start with 365
var num = 365;

function onpageload() {
	getDaysSince();
	var myVar = setInterval(myTimer, 1000/days_down);
}

function getDaysSince() {
	var now = new Date();
	var oneDay = 24*60*60*1000;
	var days = Math.round(Math.abs((now.getTime() - last_outage.getTime())/(oneDay)));
	
	document.getElementById("s").innerHTML = days;
	if (days == 1) {
		document.getElementById("d").innerHTML = "Day";
	} else {
		document.getElementById("d").innerHTML = "Days";
	}
}

function myTimer() {
	if (num > 365-days_down)
		num--;
	document.getElementById("n").innerHTML = num;
}
